$(document).ready(function () {

  $("#send-question").click(function (e) {
    e.preventDefault();

    if ($('.contact-sender').val() == "") {
          showMessage('Please type your name', 'danger');
          return;
    }

    if($('.contact-email').val() == "" || !validateEmail($('.contact-email').val())) {
      showMessage('Please type a valid email address', 'danger');
      return;
     }

     if ($('.contact-message').val() == "") {
           showMessage('Please type a message', 'danger');
           return;
     }

     var v = grecaptcha.getResponse();
     if(v.length == 0)
     {
         showMessage('Please fill Captcha', 'danger');
         //document.getElementById('g-recaptcha').innerHTML="You can't leave Captcha Code empty";
         return false;
     }

        jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: '/portfolioQuestion',
                data: $(".contact-form").serialize(),
                success: function(response){
                  showMessage(response.message, 'success');
                  $('#contact-form').trigger("reset");
                  $('#form_name').val('');
                  $('#form_email').val('');
                  $('#form_message').val('');
                },
                error: function(response){
                    handleAjaxErrors(response);
                }
        });

     });

     function showMessage(message, type) {
       $.notify(message, type);
     }

     function validateEmail($email) {
       var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
       return emailReg.test( $email );
     }

     function handleAjaxErrors(response) {
         if (response.status == 422) {
             var errorsList = '<ul>';
             var json = JSON.parse(response.responseText);
             $.each(json.errors, function (index, element) {
               showMessage(element, 'danger');
             });
         } else {
             showMessage('Something went wrong, please try again.', 'danger');
         }
     }

});
